/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.entities;

import java.sql.Date;

/**
 *
 * @author Hashan Jayakody
 * @version 1.0
 */
public class Notification {
    
    private int notificationId;
    
    private Date dateInserted;
    
    private String notificationIconPath;
    
    private String notificationMessage;

    public int getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(int notificationId) {
        this.notificationId = notificationId;
    }

    public Date getDateInserted() {
        return dateInserted;
    }

    public void setDateInserted(Date dateInserted) {
        this.dateInserted = dateInserted;
    }

    public String getNotificationIconPath() {
        return notificationIconPath;
    }

    public void setNotificationIconPath(String notificationIconPath) {
        this.notificationIconPath = notificationIconPath;
    }

    public String getNotificationMessage() {
        return notificationMessage;
    }

    public void setNotificationMessage(String notificationMessage) {
        this.notificationMessage = notificationMessage;
    }
    
    
    
}
