/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.majlis.controller;

import com.dmssw.entities.Notification;
import com.dmssw.majlis.config.AppParams;
import com.dmssw.orm.controllers.DbCon;
import com.dmssw.orm.controllers.ORMConnection;
import com.dmssw.orm.models.MajlisNotification;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.transaction.UserTransaction;
import javax.ws.rs.core.MultivaluedMap;
import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

/**
 *
 * @author Hashan Jayakody
 * @version 1.0
 */
@LocalBean
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class NotificationController {

    //private static Logger LOGGER = Logger.getLogger("InfoLogging");
    private org.apache.log4j.Logger logger;

    @Resource
    UserTransaction transaction;

    public NotificationController() {

        logger = com.dmssw.orm.config.AppParams.logger;
    }

    public ResponseData routeService(int notificationEventId, int userId, int eventId, int start, int limit) {

        ResponseData rw = new ResponseData();
        List<Notification> notificationList = new ArrayList<Notification>();
        Notification notification = new Notification();
        int result = -1;

        if (userId == 0 && notificationEventId != 0) {

            try {

                notification = getNotificationDetail(notificationEventId, start, limit);

                if (notification != null) {
                    logger.info("Notification:" + notification.getDateInserted() + "IconPath" + notification.getNotificationIconPath());
                    result = 1;
                    rw.setResponseData(notification);
                    rw.setResponseCode(result);
                }

            } catch (Exception ex) {
                result = 999;
                Logger.getLogger(NotificationController.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else if (notificationEventId == 0 && userId != 0) {
            try {

                notificationList = getNotificationList1(userId, start, limit);
                if (notificationList != null) {
                    result = 1;
                    rw.setResponseData(notificationList);
                    rw.setResponseCode(result);
                }
            } catch (Exception ex) {

                result = 999;
                Logger.getLogger(NotificationController.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else if (eventId != 0) {
            try {

                notificationList = getNotificationListbyEvent(eventId, start, limit);
                if (notificationList != null) {
                    result = 1;
                    rw.setResponseData(notificationList);
                    rw.setResponseCode(result);
                }
            } catch (Exception ex) {

                result = 999;
                Logger.getLogger(NotificationController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            result = 1;
            rw.setResponseCode(result);
            rw.setResponseData(notificationList);

        }

        return rw;

    }

    public List<Notification> getNotificationList(int userId, int start, int limit) throws SQLException, Exception {

        List<Notification> notificationList = new ArrayList<>();

        DbCon dbcon = new DbCon();
        Connection conn = dbcon.getCon();

        String tail = userId == 0 ? "USER_ID = USER_ID" : "USER_ID = " + userId;

        String sql = "SELECT EVENT_ID FROM majlis_mobile_users_event WHERE " + userId;

        ResultSet rs = dbcon.search(conn, sql);
        while (rs.next()) {

            DbCon db2 = new DbCon();

            Connection conn2 = db2.getCon();

            logger.info("Getting Event Id's");

            int eventId = rs.getInt("EVENT_ID");

            String sql2 = "SELECT * FROM majlis_notification WHERE NOTIFICATION_EVENT_ID = " + eventId + " LIMIT " + limit + " OFFSET " + start;

            ResultSet rs2 = db2.search(conn2, sql2);

            while (rs2.next()) {
                Notification notifi = new Notification();

                notifi.setDateInserted(rs2.getDate("DATE_INSERTED"));
                notifi.setNotificationIconPath(validateGroupIconPath(rs2.getString("NOTIFICATION_ICON_PATH")));
                notifi.setNotificationId(rs2.getInt("NOTIFICATION_ID"));
                notifi.setNotificationMessage(rs2.getString("NOTIFICATION_MESSAGE"));

                notificationList.add(notifi);
            }
            conn2.close();

        }

        return notificationList;
    }

    public Notification getNotificationDetail(int notificationEventId, int start, int limit) throws Exception {

        Notification notification = new Notification();
        logger.info("Getting Notification Detail of Notification Id" + notificationEventId);
        DbCon dbcon = new DbCon();
        Connection con = dbcon.getCon();

        String tail = notificationEventId == 0 ? "NOTIFICATION_ID = NOTIFICATION_ID " : "NOTIFICATION_ID = " + notificationEventId;
        String sql = "SELECT * FROM majlis_notification WHERE " + tail + " LIMIT " + limit + " OFFSET " + start;

        ResultSet rs = dbcon.search(con, sql);

        while (rs.next()) {

            notification.setDateInserted(rs.getDate("DATE_INSERTED"));
            notification.setNotificationIconPath(validateGroupIconPath(rs.getString("NOTIFICATION_ICON_PATH")));
            notification.setNotificationId(rs.getInt("NOTIFICATION_ID"));
            notification.setNotificationMessage(rs.getString("NOTIFICATION_MESSAGE"));
        }
        con.close();

        return notification;

    }

    private String validateGroupIconPath(String path) {

        String ipath = "";
        if (path != null) {
            return AppParams.MEDIA_SERVER + path;
        }
        return ipath;
    }

    public List<Notification> getNotificationListbyEvent(int eventId, int start, int limit) throws Exception {
        List<Notification> notificationList = new ArrayList<>();
        logger.info("Getting Notification List per Event ID" + eventId);

        DbCon dbcon = new DbCon();
        Connection con = dbcon.getCon();

        String sql = "SELECT * FROM majlis_notification WHERE NOTIFICATION_EVENT_ID = " + eventId + " LIMIT " + limit + " OFFSET " + start;

        ResultSet rs = dbcon.search(con, sql);

        while (rs.next()) {

            Notification notification = new Notification();
            notification.setDateInserted(rs.getDate("DATE_INSERTED"));
            notification.setNotificationIconPath(validateGroupIconPath(rs.getString("NOTIFICATION_ICON_PATH")));
            notification.setNotificationId(rs.getInt("NOTIFICATION_ID"));
            notification.setNotificationMessage(rs.getString("NOTIFICATION_MESSAGE"));

            notificationList.add(notification);
        }
        return notificationList;

    }

    public List<Notification> getNotificationList1(int userId, int start, int limit) throws Exception {

        List<Notification> notificationList = new ArrayList<>();
        DbCon dbcon = new DbCon();
        Connection conn = dbcon.getCon();

        String tail = userId == 0 ? "mmue.USER_ID = mmue.USER_ID" : "mmue.USER_ID = " + userId;

        String sql = "select mn.NOTIFICATION_ID,mn.NOTIFICATION_SYSTEM_ID,mn.NOTIFICATION_GROUP_ID,mn.NOTIFICATION_EVENT_ID,mn.NOTIFICATION_USER_ID,mn.NOTIFICATION_ICON_PATH,mn.NOTIFICATION_MESSAGE,mn.NOTIFICATION_STATUS,mn.DATE_INSERTED from majlis_mobile_users_event mmue,majlis_notification mn "
                + "where mmue.EVENT_ID=mn.NOTIFICATION_EVENT_ID and " + tail + " limit " + limit + " offset " + start;

        ResultSet rs = dbcon.search(conn, sql);

        while (rs.next()) {

            Notification notification = new Notification();
            notification.setDateInserted(rs.getDate("DATE_INSERTED"));
            notification.setNotificationIconPath(validateGroupIconPath(rs.getString("NOTIFICATION_ICON_PATH")));
            notification.setNotificationId(rs.getInt("NOTIFICATION_ID"));
            notification.setNotificationMessage(rs.getString("NOTIFICATION_MESSAGE"));

            notificationList.add(notification);
        }
        return notificationList;
    }

    public ResponseData getNotificationDetail(String notificationId,
            String notificationSystemId,
            String notificationIconPath,
            String notificationMessage,
            String dateInserted,
            String notificationEventId,
            String notificationGroupId,
            String notificationStatus,
            String notificationUserId,
            int start,
            int limit) {

        ResponseData responseData = new ResponseData();
        int result = -1;

        List<Notification> majlisNotificationList = new ArrayList<Notification>();

        DbCon dbCon = new DbCon();
        Connection connection = dbCon.getCon();

        try {
            String sql = "SELECT * FROM majlis_notification WHERE "
                    + "NOTIFICATION_ID =" + notificationId + " AND "
                    + "NOTIFICATION_SYSTEM_ID =" + notificationSystemId + " AND "
                    + "NOTIFICATION_GROUP_ID =" + notificationGroupId + " AND  "
                    + "NOTIFICATION_EVENT_ID =" + notificationEventId + " AND "
                    + "NOTIFICATION_USER_ID = " + notificationUserId + " AND "
                    + "NOTIFICATION_ICON_PATH =" + notificationIconPath + " AND "
                    + "NOTIFICATION_MESSAGE =" + notificationMessage + " AND "
                    + "NOTIFICATION_STATUS =" + notificationStatus + " AND "
                    + "DATE_INSERTED =" + dateInserted + "";

            System.out.println("SQL..." + sql);
            ResultSet rs = dbCon.search(connection, sql);

            while (rs.next()) {
                Notification notification = new Notification();

                notification.setDateInserted(rs.getDate("DATE_INSERTED"));
                notification.setNotificationIconPath(validateGroupIconPath(rs.getString("NOTIFICATION_ICON_PATH")));
                notification.setNotificationId(rs.getInt("NOTIFICATION_ID"));
                notification.setNotificationMessage(rs.getString("NOTIFICATION_MESSAGE"));

                majlisNotificationList.add(notification);
            }
            result = 1;
            responseData.setResponseData(majlisNotificationList);

        } catch (Exception ex) {
            result = 999;
            ex.printStackTrace();
        }
        responseData.setResponseCode(result);
        return responseData;

    }

    public ResponseData createNotification(MajlisNotification majlisNotification) {

        ResponseData responseData = new ResponseData();
        int result = -1;

        DbCon dbcon = new DbCon();
        Connection connection = dbcon.getCon();
        String imgPath = null;

        int notificationId = 0;
        int systemId = 0;

        if (majlisNotification.getNotificationGroupId() != null && majlisNotification.getNotificationEventId() != null
                && majlisNotification.getNotificationUserId() != null
                && majlisNotification.getNotificationMessage() != null && majlisNotification.getNotificationStatus() != null) {

            try {

                transaction.begin();
                String insertSql = "INSERT INTO majlis_notification("
                        + "NOTIFICATION_GROUP_ID,"
                        + "NOTIFICATION_EVENT_ID,"
                        + "NOTIFICATION_USER_ID,"
                        + "NOTIFICATION_ICON_PATH,"
                        + "NOTIFICATION_MESSAGE,"
                        + "NOTIFICATION_STATUS,"
                        + "DATE_INSERTED)"
                        + "VALUES(?,?,?,?,?,?,NOW())";

                PreparedStatement pr = dbcon.prepareAutoId(connection, insertSql);

                if (majlisNotification.getNotificationIconPath() != null) {
                    String url = majlisNotification.getNotificationIconPath();
                    String[] arrayUrl = url.split(com.dmssw.majlis.config.AppParams.MEDIA_SERVER + "/");

                    imgPath = arrayUrl[1];
                } else {
                    imgPath = AppParams.IMG_PATH_NOTIFICATION + "/" + "image.png";
                    imgPath = imgPath.substring(1);

                }

                pr.setInt(1, majlisNotification.getNotificationGroupId().getGroupId());
                pr.setInt(2, majlisNotification.getNotificationEventId().getEventId());
                pr.setString(3, majlisNotification.getNotificationUserId().getUserId());
                pr.setString(4, imgPath);
                pr.setString(5, majlisNotification.getNotificationMessage());
                pr.setInt(6, majlisNotification.getNotificationStatus().getCodeId());

                pr.executeUpdate();

                ResultSet gen = pr.getGeneratedKeys();

                while (gen.next()) {
                    notificationId = gen.getInt(1);
                }

                systemId = generateSystemId(notificationId);

                String sqlForUpdate = "UPDATE majlis_notification SET NOTIFICATION_SYSTEM_ID = '" + systemId + "' "
                        + "WHERE NOTIFICATION_ID='" + notificationId + "'";

                dbcon.save(connection, sqlForUpdate);

                transaction.commit();

                result = 1;
                responseData.setResponseData(notificationId);

            } catch (Exception ex) {
                result = 999;
                ex.printStackTrace();

            }

        } else {
            result = 999;
            logger.info("Null entries");
        }

        responseData.setResponseCode(result);
        return responseData;
    }

    public ResponseData updateNotification(MajlisNotification majlisNotification) {

        ResponseData responseData = new ResponseData();
        int result = -1;

        DbCon dbCon = new DbCon();

        Connection connection = dbCon.getCon();

        int notificationId = majlisNotification.getNotificationId();
        String imgPath = null;

        if (notificationId != 0 && majlisNotification.getNotificationGroupId() != null && majlisNotification.getNotificationEventId() != null
                && majlisNotification.getNotificationUserId() != null && majlisNotification.getNotificationIconPath() != null
                && majlisNotification.getNotificationMessage() != null && majlisNotification.getNotificationStatus() != null) {

            try {

                String updateSql = "UPDATE majlis_notification SET "
                        + "NOTIFICATION_GROUP_ID =?,"
                        + "NOTIFICATION_EVENT_ID = ?,"
                        + "NOTIFICATION_USER_ID =?,"
                        + "NOTIFICATION_ICON_PATH =?,"
                        + "NOTIFICATION_MESSAGE =?,"
                        + "NOTIFICATION_STATUS =? WHERE NOTIFICATION_ID= " + notificationId;

                PreparedStatement pr = dbCon.prepare(connection, updateSql);

                String url = majlisNotification.getNotificationIconPath();
                String[] arrayUrl = url.split(com.dmssw.majlis.config.AppParams.MEDIA_SERVER + "/");

                imgPath = arrayUrl[1];

                pr.setInt(1, majlisNotification.getNotificationGroupId().getGroupId());
                pr.setInt(2, majlisNotification.getNotificationEventId().getEventId());
                pr.setString(3, majlisNotification.getNotificationUserId().getUserId());
                pr.setString(4, imgPath);
                pr.setString(5, majlisNotification.getNotificationMessage());
                pr.setInt(6, majlisNotification.getNotificationStatus().getCodeId());

                pr.executeUpdate();

                result = 1;
                responseData.setResponseData(majlisNotification);
            } catch (Exception ex) {
                result = 999;
                ex.printStackTrace();
            }

        } else {
            result = 999;
            logger.info("Null Entries...");
        }

        responseData.setResponseCode(result);
        return responseData;
    }

    /**
     * Upload image to the server and return the URL of the image
     *
     * @param input
     * @return
     */
    public ResponseData saveNotificationIcon(MultipartFormDataInput input) {

        int result = -1;
        ResponseData rd = new ResponseData();

        Map<String, List<InputPart>> uploadForm = input.getFormDataMap();

        try {

            logger.info("start saveimage method............");

            transaction.begin();

            List<InputPart> inputParts = uploadForm.get("file");

            for (InputPart inputPart : inputParts) {

                MultivaluedMap<String, String> headers = inputPart.getHeaders();
                try {

                    java.io.InputStream inputStream = inputPart.getBody(java.io.InputStream.class, null);
                    byte[] bytes = IOUtils.toByteArray(inputStream);
                    String saveLocation = com.dmssw.majlis.config.AppParams.IMG_PATH;

                    Random rnd = new Random();

                    int randomId = 100000 + rnd.nextInt(900000);

                    String pathForDb = com.dmssw.majlis.config.AppParams.IMG_PATH_NOTIFICATION + "/" + randomId + ".jpg";

                    File dir = new File(saveLocation + com.dmssw.majlis.config.AppParams.IMG_PATH_NOTIFICATION);// Creating the directory if the directory is not created.

                    dir.mkdir();
                    logger.info("image path is " + saveLocation);

                    FileOutputStream stream = new FileOutputStream(saveLocation + pathForDb);

                    try {
                        stream.write(bytes);
                    } finally {
                        stream.close();
                    }

                    result = 1;
                    rd.setResponseData(com.dmssw.majlis.config.AppParams.MEDIA_SERVER + pathForDb);
                    transaction.commit();
                } catch (IOException e) {
                    transaction.rollback();
                    result = 999;
                    logger.error("Cannot save the image " + e.getMessage());

                }
            }

        } catch (Exception ex) {

            logger.error("EJB Exception " + ex.getMessage());

        }
        rd.setResponseCode(result);

        return rd;

    }

    /**
     * This method creates a new system ID for a new group
     *
     * @param groupId
     * @return the new system Id
     */
    private int generateSystemId(int groupId) {
        int systemId = 0;
        Date currentDate = new Date();

        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDate);
        String year = String.valueOf(cal.get(Calendar.YEAR));
        String month = String.valueOf(cal.get(Calendar.MONTH) + 1);
        systemId = Integer.parseInt(year + month + groupId);
        return systemId;

    }

    public ResponseData getAllNotifications(int start, int limit) {

        ResponseData responseData = new ResponseData();
        int result = -1;

        ORMConnection connection = new ORMConnection();
        List<Object> list;
        List<MajlisNotification> notificationList = new ArrayList<MajlisNotification>();
        MajlisNotification majlisNotification = new MajlisNotification();

        String hql = "SELECT m FROM MajlisNotification m";

        list = connection.hqlGetResultsWithLimit(hql, start, limit);

        for (Object object : list) {
            majlisNotification = (MajlisNotification) object;
            majlisNotification.setNotificationIconPath(validateGroupIconPath(majlisNotification.getNotificationIconPath()));
            notificationList.add(majlisNotification);
        }

        result = 1;
        responseData.setResponseData(notificationList);

        responseData.setResponseCode(result);
        return responseData;

    }

    public ResponseData createNotificationDetail(int notficationId, String type) {

        ResponseData responseData = new ResponseData();
        int result = -1;

        DbCon dbCon = new DbCon();
        Connection connection = dbCon.getCon();
        String sql = null;

        if (getCodeId(type) != 0) {
            int codeId = getCodeId(type);
            sql = "INSERT INTO majlis_notification_send (NOTIFICATION_ID,NOTIFICATION_SEND_TO) VALUES (" + notficationId + "," + codeId + ")";
        }

        try {
            dbCon.save(connection, sql);
            result = 1;
        } catch (Exception ex) {
            result = 999;
            ex.printStackTrace();
        }

        finally{
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(NotificationController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        responseData.setResponseCode(result);
        return responseData;
    }

    public int getCodeId(String type) {
        int codeId = 0;
        DbCon dbCon = new DbCon();
        Connection connection = dbCon.getCon();

        try {
            String sql = "SELECT CODE_ID from majlis_md_code WHERE CODE_TYPE='NOTIFICATION' and CODE_LOCALE='" + com.dmssw.majlis.config.AppParams.LOCALE + "' and CODE_MESSAGE='" + type + "'";

            ResultSet rs = dbCon.search(connection, sql);

            while (rs.next()) {

                codeId = rs.getInt("CODE_ID");
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }finally{
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(NotificationController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return codeId;
    }

}
