/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.majlis.service;

import com.dmssw.majlis.controller.NotificationController;
import com.dmssw.orm.models.MajlisNotification;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

/**
 *
 * @author Hashan Jayakody
 */
@Path("/notification")
public class NotificationService {

    @EJB
    NotificationController notificationController;

    @GET
    @Path("/getNotificationList")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getNotificationList(@QueryParam("userid") @DefaultValue("0") int userId,
            @QueryParam("notificationId") @DefaultValue("0") int notificationEventId,
            @QueryParam("eventid") @DefaultValue("0") int eventId,
            @QueryParam("start") @DefaultValue("0") int start, @QueryParam("limit") @DefaultValue("25") int limit) {

        return Response.status(Response.Status.OK).entity(notificationController.routeService(notificationEventId, userId, eventId, start, limit)).build();
    }

    /**
     * Majlis-NotificationManagement-1.0/service/notification/saveNotificationIcon
     *
     * @param input
     * @param headers
     * @return
     */
    @POST
    @Path("/saveNotificationIcon")
    @Consumes(MediaType.MEDIA_TYPE_WILDCARD)
    @Produces(MediaType.APPLICATION_JSON)
    public Response saveNotificationIcon(MultipartFormDataInput input, @Context HttpHeaders headers) {

        headers.getRequestHeaders().forEach((key, val) -> {

            for (String string : val) {
                System.out.println("key " + key + " val... " + val);
            }

        });

        return Response.status(Response.Status.OK).entity(notificationController.saveNotificationIcon(input)).build();
    }

    /**
     * Majlis-NotificationManagement-1.0/service/notification/createNotification
     *
     * @param input
     * @param headers
     * @return
     */
    @POST
    @Path("/createNotification")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createNotification(MajlisNotification majlisNotification) {

        return Response.status(Response.Status.OK).entity(notificationController.createNotification(majlisNotification)).build();
    }

    /**
     * Majlis-NotificationManagement-1.0/service/notification/updateNotification
     *
     * @param input
     * @param headers
     * @return
     */
    @POST
    @Path("/updateNotification")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateNotification(MajlisNotification majlisNotification) {

        return Response.status(Response.Status.OK).entity(notificationController.updateNotification(majlisNotification)).build();
    }

    @GET
    @Path("/getAllNotificationList")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getNotificationList( @QueryParam("start") @DefaultValue("0") int start, @QueryParam("limit") @DefaultValue("25") int limit) {

        return Response.status(Response.Status.OK).entity(notificationController.getAllNotifications(start,limit)).build();
    }

    @GET
    @Path("/getNotificationDetail")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getNotificationDetail(@QueryParam("notificationId") @DefaultValue("NOTIFICATION_ID") String notificationId,
            @QueryParam("notificationSystemId") @DefaultValue("NOTIFICATION_SYSTEM_ID") String notificationSystemId,
            @QueryParam("notificationIconPath") @DefaultValue("NOTIFICATION_ICON_PATH") String notificationIconPath,
            @QueryParam("notificationMessage") @DefaultValue("NOTIFICATION_MESSAGE") String notificationMessage,
            @QueryParam("dateInserted") @DefaultValue("DATE_INSERTED") String dateInserted,
            @QueryParam("notificationEventId") @DefaultValue("NOTIFICATION_EVENT_ID") String notificationEventId,
            @QueryParam("notificationGroupId") @DefaultValue("NOTIFICATION_GROUP_ID") String notificationGroupId,
            @QueryParam("notificationStatus") @DefaultValue("NOTIFICATION_STATUS") String notificationStatus,
            @QueryParam("notificationUserId") @DefaultValue("NOTIFICATION_USER_ID") String notificationUserId,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("25") int limit) {

        System.out.println("Starting>>>>");
        return Response.status(Response.Status.OK).entity(notificationController.getNotificationDetail(notificationId, 
                notificationSystemId, notificationIconPath, notificationMessage, dateInserted, notificationEventId, notificationGroupId, notificationStatus, notificationUserId, start, limit)).build();
    }
    
    @POST
    @Path("/createNotificationDetail")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createNotificationDetail(@HeaderParam("notificationId") int notificationId, @HeaderParam("sendTo") String sendTo) {

        return Response.status(Response.Status.OK).entity(notificationController.createNotificationDetail(notificationId,sendTo)).build();
    }

}
